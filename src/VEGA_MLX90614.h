#ifndef MLX90614_VEGA_
#define MLX90614_VEGA_

#include<Arduino.h>

#define MLX90614_WRITE_ADDR     0xB4
#define MLX90614_READ_ADDR      0xB5
#define MLX90614_AMBIENT_TEMP   0x06
#define MLX90614_TARGET_TEMP    0x07

class VEGA_MLX90614 {

    public:
    VEGA_MLX90614(uint8_t _sda, uint8_t _scl);
    void mlx90614Start(void);
    void mlx90614Stop(void);
    void mlx90614Restart(void);
    void writebyte( unsigned char data);
    unsigned char readbyte(void);
    double mlx90614ReadTempC(unsigned char reg);
    double mlx90614ReadAmbientTempC(void);
    double mlx90614ReadTargetTempC(void);

    private:
    uint8_t sdaPin;
    uint8_t sclPin;
};

#endif  /* MLX90614_VEGA_ */
