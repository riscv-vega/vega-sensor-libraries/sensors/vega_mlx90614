 * Library Name : VEGA_MLX90614 by VEGA-Processor
 
This is a library for the MLX90614 temperature sensor SPECIFICALLY
FOR USE WITH VEGA ARIES Boards

The MLX90614 is an infrared thermometer for non-contact temperature measurements. These sensors use I2C to communicate,
In this library we are using GPIO pin as I2C which supports repeated start.

 * MLX90614 Datasheet: https://www.sparkfun.com/datasheets/Sensors/Temperature/MLX90614_rev001.pdf
 * ARIES Pinmap: https://vegaprocessors.in/files/PINOUT_ARIES%20V2.0_.pdf

Check out the above links for mlx datasheet and ARIES Pinmap. 
 
 * Connections:
 * MLX90614     Aries Board
 * VIN      -   3.3V
 * GND      -   GND
 * SDA      -   GPIO-3
 * SCL      -   GPIO-4
 * You can use any GPIO pins 

Other Useful links:

 * Official site: https://vegaprocessors.in/
 * YouTube channel: https://www.youtube.com/@VEGAProcessors

